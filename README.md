# Smart city project

## Objectifs

- Identifier et reconnaître les visages des personnes présentes dans une scène
- Détecter les feux ou fumées dans des forêts proches de la ville intelligente à partir d’images capturées via la caméra.
- Détecter la présence d’un ou plusieurs objets suspects ou non autorisés (ex. bâtons, sacs isolés, armes, etc.) dans une scène filmée par la caméra.
- (option) Reconnaitre et classifier les mouvements (marche, marche rapide, chute, dispute, etc.) dans une scène filmée par la caméra.



